package facci.diegomera.primerapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText input;
    Button botoned;
    TextView resultado;
    RadioButton celsius, fahrenheit;
    Double inputNumber;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i("MainActivity", "Diego Raúl Mera Palma");

        botoned = (Button) findViewById(R.id.botoncv);
        resultado = (TextView) findViewById(R.id.resultado);
        input = (EditText) findViewById(R.id.input);
        celsius = (RadioButton) findViewById(R.id.celsius);
        fahrenheit = (RadioButton) findViewById(R.id.fahrenheit);
        botoned.setOnClickListener( new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                String inputText = input.getText().toString();

                if (inputText.isEmpty()) {
                    resultado.setText("Ingresa la cantidad.");
                } else {
                    inputNumber = Double.parseDouble(inputText);
                    if (inputNumber <= 0) {
                        resultado.setText("La cantidad debe ser mayor a 0.");
                    } else {
                        if (celsius.isChecked() || fahrenheit.isChecked()) {
                            if (celsius.isChecked()) {
                                Double r = (inputNumber - 32) * 5/9 ;
                                resultado.setText(String.valueOf(r) + " C°");
                            } else {
                                Double r = (inputNumber * 9) /5 + 32 ;
                                resultado.setText(String.valueOf(r) + " F°");
                            }
                        } else {
                            resultado.setText("Selecciona el tipo de conversión");
                        }
                    }
                }
            }
        });
    }
}
